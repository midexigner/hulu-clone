import React, { useState } from 'react';
import './App.css';
import Header from './components/Header/Header';
import Nav from './components/Nav/Nav';
import Results from './components/Results/Results';
import requests from './services/requests';

function App() {
  const [selectedOption, setselectedOption] = useState(requests.fetchActionMovies)

  return (
    <div className="app">
      {/* Header */}
      <Header />
      {/* Nav */}
      <Nav setselectedOption={setselectedOption}/>
      {/* Results */}
      <Results selectedOption={selectedOption}/>
     
    </div>
  );
}

export default App;
